<?php

class Db extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	// Insert registration data in database
	public function registration_insert($data)
	{

		// Query to check whether email already exist or not
		$condition = "email =" . "'" . $data['email'] . "'";
		$this->db->select('*');
		$this->db->from('uporabnik');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 0) {

			$entry['ime'] = $data['ime'];
			$entry['priimek'] = $data['priimek'];
			$entry['telefon'] = $data['telefon'];
			$entry['email'] = $data['email'];
			$entry['geslo'] = $data['geslo'];

			$entry2['ime'] = $data['role'];

			// Query to insert data in database
			$this->db->insert('uporabnik', $entry);
			$entry2['user_id'] = $this->db->insert_id();
			if ($this->db->affected_rows() > 0) {
				$this->db->insert('vloga',$entry2);
				return true;
			}
		} else {
			return false;
		}
	}

	// Read data using username and password
	public function login($data)
	{

		$condition = "email =" . "'" . $data['email'] . "' AND " . "geslo =" . "'" . $data['geslo'] . "'";
		$this->db->select('*');
		$this->db->from('uporabnik');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	// Read data from database to show data in admin page
	public function read_user_information($email)
	{

		$condition = "email =" . "'" . $email . "'";
		$this->db->select('*');
		$this->db->from('uporabnik');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}

	public function insert($data)
	{

		$data1['objava'] = $data['objava'];
		$data1['objavil'] = $data['user_id'];

		$condition = "ime =" . "'" . $data['predmet'] . "'";
		$this->db->select('id');
		$this->db->from('predmet');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		$data1['predmet_id'] = $query->result()[0]->id;

		return $this->db->insert('objava', $data1);
	}

	public function delete_post($id)
	{
		$this->db->where("id", $id);
		$this->db->delete("objava");
	}

	public function update_post($data, $id)
	{
		$this->db->set($data);
		$this->db->where("id", $id);
		$this->db->update("objava");
	}

	public function getAllPosts()
	{

		$selection = "o.predmet_id = p.id";
		$this->db->select('o.id, o.objava, o.predmet_id, o.objavil, p.ime, p.program, p.letnik,');
		$this->db->from('objava o, predmet p');
		$this->db->where($selection);
		$query = $this->db->get();

		return $query->result();
	}


	public function getUser($data)
	{
		$selection = "ime, " . "priimek";
		$condition = "id =" . "'" . $data->user_id . "'";
		$this->db->select($selection);
		$this->db->from('uporabnik');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();;
	}

	public function getUserID($data)
	{
		$condition = "id =" . "'" . $data . "'";
		$this->db->select('objavil');
		$this->db->from('objava');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		return $query->result();
	}

	public function getUserIDfromemail($data)
	{

		$condition = "email =" . "'" . $data . "'";
		$this->db->select('id');
		$this->db->from('uporabnik');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result();;
	}

	public function checkrole($id) {

		$condition = 'u.id = v.user_id and u.id =' . $id;
		$this->db->select('v.ime');
		$this->db->from('uporabnik u, vloga v');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->result()[0]->ime;
		  
	}

	public function getCourses() {

		$this->db->select('*');
		$this->db->from('predmet');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_post($slug = FALSE) {

		$condition = 'p.id = o.predmet_id and o.objavil = u.id and o.id =' . $slug;
		$this->db->select('u.ime AS imee, u.priimek, u.telefon, u.email, o.id, o.objava, o.objavil, p.ime, p.program, p.letnik');
		$this->db->from('uporabnik u, objava o, predmet p');
		$this->db->where($condition);
		$query = $this->db->get();
		return $query->result();
        }

		public function searchposts($keyword)
	{
		$selection = 'o.id, o.objava, o.predmet_id, o.objavil, p.ime, p.program, p.letnik';
		$fromtables = 'objava o, predmet p';
		$condition = "o.predmet_id = p.id and (p.ime like '%" . $keyword . "%' or o.objava like '%" . $keyword . "%')";
		$this->db->select($selection);
		$this->db->from($fromtables);
		$this->db->where($condition);
		$postsquery = $this->db->get();

		$postResult = $postsquery->result();

		return $postResult;

	}

	public function getIzvajalci($userid)
	{

		// Posts
		$this->db->select('u.ime, u.priimek, u.id');
		$this->db->from('uporabnik u, vloga v');
		$this->db->where('v.ime = "instruktor" and v.user_id = u.id');
		$query = $this->db->get();
		
		$postResult = $query->result_array();

		$posts_arr = array();
		foreach ($postResult as $post) {
			$id = $post['id'];
			$ime = $post['ime'];
			$priimek = $post['priimek'];			

			// User rating
			$this->db->select('stars');
			$this->db->from('ocena');
			$this->db->where("user_id", $userid);
			$this->db->where("izvajalecid", $id);
			$userRatingquery = $this->db->get();

			$userpostResult = $userRatingquery->result_array();

			$userRating = 0;
			if (count($userpostResult) > 0) {
				$userRating = $userpostResult[0]['stars'];
			}

			// Average rating
			$this->db->select('ROUND(AVG(stars),1) as averageRating');
			$this->db->from('ocena');
			$this->db->where("izvajalecid", $id);
			$ratingquery = $this->db->get();

			$postResult = $ratingquery->result_array();

			$rating = $postResult[0]['averageRating'];

			if ($rating == '') {
				$rating = 0;
			}

			$posts_arr[] = array("id" => $id, "ime" => $ime, "priimek" => $priimek, "rating" => $userRating, "averagerating" => $rating);
		}

		return $posts_arr;
	}

	public function userRating($userid, $izvajalecid, $rating)
	{
		$this->db->select('*');
		$this->db->from('ocena');
		$this->db->where("izvajalecid", $izvajalecid);
		$this->db->where("user_id", $userid);
		$userRatingquery = $this->db->get();

		$userRatingResult = $userRatingquery->result_array();
		if (count($userRatingResult) > 0) {

			$postRating_id = $userRatingResult[0]['id'];
			// Update
			$value = array('stars' => $rating);
			$this->db->where('id', $postRating_id);
			$this->db->update('ocena', $value);
		} else {
			$userRating = array(
				"izvajalecid" => $izvajalecid,
				"user_id" => $userid,
				"stars" => $rating
			);

			$this->db->insert('ocena', $userRating);
		}

		// Average rating
		$this->db->select('ROUND(AVG(stars),1) as averageRating');
		$this->db->from('ocena');
		$this->db->where("izvajalecid", $izvajalecid);
		$ratingquery = $this->db->get();

		$postResult = $ratingquery->result_array();

		$rating = $postResult[0]['averageRating'];

		if ($rating == '') {
			$rating = 0;
		}
		return $rating;
	}


}
