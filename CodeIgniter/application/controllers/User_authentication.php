<?php 

class User_authentication extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url_helper');

		// Load form helper library
		$this->load->helper('form');

		// Load form validation library
		$this->load->library('form_validation');

		// Load session library
		$this->load->library('session');

		// Load database
		$this->load->model('Db');


	}

	// Show login page
	public function index()
	{
		if (isset($this->session->userdata['logged_in'])){
			$user = $this->session->userdata['logged_in']['email'];
			$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
			$data['checkrole'] = $this->Db->checkrole($user_id);
		}

		$data['logiran'] = isset($this->session->userdata['logged_in']);
		$this->load->view('templates/header', $data);
		$this->load->view('user_authentication/login');
		$this->load->view('templates/footer');
	}

	// Show registration page
	public function show()
	{
		if (isset($this->session->userdata['logged_in'])){
			$user = $this->session->userdata['logged_in']['email'];
			$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
			$data['checkrole'] = $this->Db->checkrole($user_id);
		}
		$data['logiran'] = isset($this->session->userdata['logged_in']);
		$this->load->view('templates/header', $data);
		$this->load->view('user_authentication/register');
		$this->load->view('templates/footer');
	}


	// Validate and store registration data in database
	public function signup()
	{
		if (isset($this->session->userdata['logged_in'])){
			$user = $this->session->userdata['logged_in']['email'];
			$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
			$data['checkrole'] = $this->Db->checkrole($user_id);
		}
		$data['logiran'] = isset($this->session->userdata['logged_in']);

		// Check validation for user input in SignUp form
		$this->form_validation->set_rules('ime', 'Ime', 'trim|required');
		$this->form_validation->set_rules('priimek', 'Priimek', 'trim|required');
		$this->form_validation->set_rules('telefon', 'Telefon', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('geslo', 'Geslo', 'trim|required');
		$this->form_validation->set_rules('role', 'Role', 'trim|required');


		if ($this->form_validation->run() == FALSE) {
			$data['logiran'] = isset($this->session->userdata['logged_in']);

			$this->load->view('templates/header',$data);
			$this->load->view('user_authentication/register');
			$this->load->view('templates/footer');
		} else {
			$data = array(
				'ime' => $this->input->post('ime'),
				'priimek' => $this->input->post('priimek'),
				'telefon' => $this->input->post('telefon'),
				'email' => $this->input->post('email'),
				'geslo' => $this->input->post('geslo'),
				'role' => $this->input->post('role'),

			);
			$result = $this->Db->registration_insert($data);
			if ($result == TRUE) {
				$data['logiran'] = isset($this->session->userdata['logged_in']);

				$data['message_display'] = 'Registration Successfully !';
				$this->load->view('templates/header',$data);
				$this->load->view('user_authentication/login', $data);
				$this->load->view('templates/footer');
			} else {
				$data['logiran'] = isset($this->session->userdata['logged_in']);
				$data['message_display'] = 'Email already in system!';
				$this->load->view('templates/header',$data);
				$this->load->view('user_authentication/register');
				$this->load->view('templates/footer');
			}
		}
	}


	// Check for user login process
	public function signin()
	{
		if (isset($this->session->userdata['logged_in'])){
			$user = $this->session->userdata['logged_in']['email'];
			$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
			$data['checkrole'] = $this->Db->checkrole($user_id);
		}

		$data['logiran'] = isset($this->session->userdata['logged_in']);
		$this->form_validation->set_rules('email', 'email', 'trim|required');
		$this->form_validation->set_rules('geslo', 'Geslo', 'trim|required');

		$data = array(
			'email' => $this->input->post('email'),
			'geslo' => $this->input->post('geslo')
		);
		$result = $this->Db->login($data);
		if ($result == TRUE) {

			$email = $this->input->post('email');
			$result = $this->Db->read_user_information($email);
			if ($result != false) {
				$session_data = array(
					'ime' => $result[0]->ime,
					'priimek' => $result[0]->priimek,
					'email' => $result[0]->email,
				);
				// Add user data in session
				$data = array('error_message' => 'Signin OK');
				$this->session->set_userdata('logged_in', $session_data);
				$data['logiran'] = isset($this->session->userdata['logged_in']);

				redirect('https://www.studenti.famnit.upr.si/~89171099/Implementacija/CodeIgniter/index.php/features/index');
			}
		} else {
			$data = array(
				'error_message' => 'Invalid Username or Password'
			);
			$data['logiran'] = isset($this->session->userdata['logged_in']);

			$this->load->view('templates/header', $data);
			$this->load->view('user_authentication/login', $data);
			$this->load->view('templates/footer');
		}
	}

	public function logout()
	{
		$data['logiran'] = isset($this->session->userdata['logged_in']);
		// Removing session data
		$sess_array = array(
			'username' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['logiran'] = isset($this->session->userdata['logged_in']);
		$data['message_display'] = 'Successfully Logout';
		$this->load->view('templates/header', $data);
		$this->load->view('user_authentication/login', $data);
		$this->load->view('templates/footer');
	}

}
