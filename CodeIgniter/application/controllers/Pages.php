<?php
class Pages extends CI_Controller {

	public function view($page)
	{
		$this->load->library('form_validation');
		$this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->model('Db');


		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}

		$data['courses'] = $this->Db->getCourses();
		$data["title"] = $page;
		$data['logiran'] = isset($this->session->userdata['logged_in']);

		if (isset($this->session->userdata['logged_in'])){
			$user = $this->session->userdata['logged_in']['email'];
			$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
			$data['checkrole'] = $this->Db->checkrole($user_id);
		}
	
		$this->load->view('templates/header',$data);
		$this->load->view('pages/'. $page);
		$this->load->view('templates/footer');
	}

}
