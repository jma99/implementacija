<?php
class Ocena extends CI_Controller {

    public function __construct()
	{

		parent::__construct();

		$this->load->helper('url');

		// Load form helper library
		$this->load->helper('form');

		// Load form validation library
		$this->load->library('form_validation');

		// Load session library
		$this->load->library('session');

		// Load database
		$this->load->model('Db');
	}

    public function index()
	{

		$data = array();

		$user = $this->session->userdata['logged_in']['email'];
		$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
        $data['checkrole'] = $this->Db->checkrole($user_id);

		$data['logiran'] = isset($this->session->userdata['logged_in']);

		if (!isset($this->session->userdata['logged_in'])) {
			$data['message_display'] = 'Signin to view this page!';
			$this->load->view('templates/header', $data);
			$this->load->view('user_authentication/login', $data);
			$this->load->view('templates/footer');
			return;
		}
		$data['logiran'] = isset($this->session->userdata['logged_in']);


		// Userid
		$user = $this->session->userdata['logged_in']['email'];
		$userid = $this->Db->getUserIDfromemail($user)[0]->id;

		// Fetch all records
		$izvajalci = $this->Db->getIzvajalci($userid);
		$this->load->view('templates/header', $data);
		$this->load->view('features/grade', ['izvajalci' => $izvajalci]);
		$this->load->view('templates/footer');

	}

    public function updateRating()
	{

		// userid
		$user = $this->session->userdata['logged_in']['email'];
		$userid = $this->Db->getUserIDfromemail($user)[0]->id;

		// POST values
		$izvajalecid = $this->input->post('izvajalecid');
		$stars = $this->input->post('rating');

		// Update user rating and get Average rating of a post
		$averageRating = $this->Db->userRating($userid, $izvajalecid, $stars);

		echo $averageRating;
		exit;
	}



}