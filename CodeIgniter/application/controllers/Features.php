<?php

    class Features extends CI_Controller {

        public function __construct()
	{

		parent::__construct();

		$this->load->helper('url');

		// Load form helper library
		$this->load->helper('form');

		// Load form validation library
		$this->load->library('form_validation');

		// Load session library
		$this->load->library('session');

		// Load database
		$this->load->model('Db');
	}

    public function index()
	{

		if (isset($this->session->userdata['logged_in'])){
			$user = $this->session->userdata['logged_in']['email'];
			$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
			$data['checkrole'] = $this->Db->checkrole($user_id);
		}
    
		$data['logiran'] = isset($this->session->userdata['logged_in']);

		if (!isset($this->session->userdata['logged_in'])) {
			$data['message_display'] = 'Signin to view this page!';
			$this->load->view('templates/header', $data);
			$this->load->view('user_authentication/login', $data);
			$this->load->view('templates/footer');
			return;
		}
		$data['logiran'] = isset($this->session->userdata['logged_in']);

        $posts = $this->Db->getAllPosts();
		$this->load->view('templates/header', $data);
		$this->load->view('features/feature', ['posts' => $posts]);
		$this->load->view('templates/footer');
	}

    public function post() {

		if (isset($this->session->userdata['logged_in'])){
			$user = $this->session->userdata['logged_in']['email'];
			$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
			$data['checkrole'] = $this->Db->checkrole($user_id);
		}

        $user = $this->session->userdata['logged_in']['email'];
		$user_id = $this->Db->getUserIDfromemail($user)[0]->id;

        $this->form_validation->set_rules('objava', 'Objava', 'trim|required');
		$this->form_validation->set_rules('predmet', 'Predment', 'trim|required');

		if ($this->form_validation->run() === FALSE) {

			$this->index();

		} else {

			$data = array(
				'objava' => $this->input->post('objava'),
                'predmet' => $this->input->post('predmet'),
                'user_id' => $user_id,
			);

			$this->form_validation->set_error_delimiters();

			if ($this->Db->insert($data)) {
				redirect('https://www.studenti.famnit.upr.si/~89171099/Implementacija/CodeIgniter/index.php/features/index');
			} else {
				redirect('https://www.studenti.famnit.upr.si/~89171099/Implementacija/CodeIgniter/index.php/features/index');
			}
			
		}
    }

	public function view($slug = NULL)
        {

			if (isset($this->session->userdata['logged_in'])){
				$user = $this->session->userdata['logged_in']['email'];
				$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
				$data['checkrole'] = $this->Db->checkrole($user_id);
			}

			$data['logiran'] = isset($this->session->userdata['logged_in']);
			$user = $this->session->userdata['logged_in']['email'];
			$data['user_id'] = $this->Db->getUserIDfromemail($user)[0]->id;

			
                $data['post'] = $this->Db->get_post($slug);
                $data["title"] = "Individual Post";
                $this->load->view('templates/header',$data);
                $this->load->view('features/view',$data);
                $this->load->view('templates/footer');
        }

	public function delete($postid)
		{

			if (isset($this->session->userdata['logged_in'])){
				$user = $this->session->userdata['logged_in']['email'];
				$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
				$data['checkrole'] = $this->Db->checkrole($user_id);
			}

		$data['logiran'] = isset($this->session->userdata['logged_in']);

		$u = $this->session->userdata['logged_in']['email'];
		$check = $this->Db->getUserIDfromemail($u)[0]->id;
		$check2 = $this->Db->getUserID($postid)[0]->objavil;

		if ($check === $check2) {
			$this->Db->delete_post($postid);
			redirect('https://www.studenti.famnit.upr.si/~89171099/Implementacija/CodeIgniter/index.php/features/index');
		} else {
			echo "You don't have permission to delete this item!";
		}

	}

	public function edit ($slug) {

		if (isset($this->session->userdata['logged_in'])){
			$user = $this->session->userdata['logged_in']['email'];
			$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
			$data['checkrole'] = $this->Db->checkrole($user_id);
		}

		$data['logiran'] = isset($this->session->userdata['logged_in']);
		$data['post'] = $this->Db->get_post($slug);

		if(!isset($this->session->userdata['logged_in'])){
				$data['message_display'] = 'Signin to view this page!';
				$this->load->view('templates/header');
				$this->load->view('user_authentication/login', $data);
				$this->load->view('templates/footer');
				return;
		}

		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Update a news item';

		$this->form_validation->set_rules('objava', 'Objava', 'required');

		if ($this->form_validation->run() === FALSE)
		{
				$data['news_item'] = $this->Db->get_post($slug);

				$this->load->view('templates/header', $data);
				$this->load->view('features/edit', $data);
				$this->load->view('templates/footer');

		}
		else
		{
				$d["objava"] = $this->input->post("objava");
				$this->Db->update_post($d, $slug);
				$this->view($slug);
			}
}
public function display($posts)

	{

		if (isset($this->session->userdata['logged_in'])){
			$user = $this->session->userdata['logged_in']['email'];
			$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
			$data['checkrole'] = $this->Db->checkrole($user_id);
		}

		$user = $this->session->userdata['logged_in']['email'];
		$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
        $data['checkrole'] = $this->Db->checkrole($user_id);
		$data['logiran'] = isset($this->session->userdata['logged_in']);

		$this->load->view('templates/header', $data);
		$this->load->view('features/feature', ['posts' => $posts]);
		$this->load->view('templates/footer');
	}



public function searchfor()
	{

		if (isset($this->session->userdata['logged_in'])){
			$user = $this->session->userdata['logged_in']['email'];
			$user_id = $this->Db->getUserIDfromemail($user)[0]->id;
			$data['checkrole'] = $this->Db->checkrole($user_id);
		}
		
		$keyword = $this->input->post('text');

		$posts = $this->Db->searchposts($keyword);
		
		$response = $this->display($posts);
		
		echo $response;
	}
}
?>