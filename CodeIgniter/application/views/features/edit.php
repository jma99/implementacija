<?php
if (!isset($this->session->userdata['logged_in'])) {
	$data['message_display'] = 'Signin to view this page!';
	$this->load->view('user_authentication/login', $data);
	return;
} ?>
<section style="background: #214a80; padding-top: 10%;">
<div class="col" style="width: 60%; margin: auto">
	<div class="col">
		<div class="block-heading" align="center">
			<h2 class="text-info">Objava</h2>
			<p><?php echo "<div class='error_msg'>";
				echo validation_errors();
				echo "</div>";
				if (isset($error_message)) {
					echo $error_message;
				}; ?></p>
		</div>
	</div>

	<div>
		<div class="container">
			<div class="row">
				<div class="col">
					<h1 class="mt-4"><?php echo $post[0]->ime; ?></h1>
					<hr>
					<p class="lead">
						Izvajalec: <?php echo $post[0]->imee . ' ' . $post[0]->priimek; ?>
                                        </p>
                                        <p class="lead">
                                                Telefon: <?php echo $post[0]->telefon ?>
                                        </p>
                                        <p class="lead">      
                                                E-mail: <?php echo $post[0]->email?>
                                        </p>
					
				</div>
			</div>
		</div>
	</div>

	<?php echo form_open('features/edit/'.$post[0]->id) ?>

	<div class="form-group" id="form"><label for="objava">Opis</label>
		<br/>
		<?php
		$data2 = array(
				'type' => 'text',
				'name' => 'objava',
				'class' => 'form-control item',
				'value' => $post[0]->objava,
		);
		echo form_input($data2); ?>
		<br/>
	</div>
	<div class="upload_btn">
	<?php
	$data5 = array(
		'type' => 'submit',
		'name' => 'submit',
		'class' => 'btn btn-primary btn-block',
		'value' => 'Spremeni',
	);
	echo form_submit($data5);
	echo form_close();
	?>
</div><br>
</section>