<section class="hero-banner d-flex align-items-center">
        <div class="container text-center">
            <h2>Grade</h2>
        </div>
    </section>
        <!--================Service  Area =================-->
        <section class="service-area" style="padding-top: 2%">
        <div class="container">
            <div class="row">

			
                <?php foreach ($izvajalci as $izvajalec): ?>
                        <!-- Single service -->
                        <div style="width: 100%">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <h1 class="mt-4"><?php echo $izvajalec['ime'] . ' ' . $izvajalec['priimek']; ?></h1>
                                    <hr>
                                    <div class="post">
										<div class="post-action">
											<!-- Rating Bar -->
											<input id="izvajalec_<?= $izvajalec['id']?>" value='<?= $izvajalec['rating'] ?>'
												   class="ratingbar rating" data-min="0" data-max="5"
												   data-step="1">
											<!-- Average Rating -->
											<div>Average Rating: <span
														id='averagerating_<?= $izvajalec['id'] ?>'><?= $izvajalec['averagerating'] ?></span>
											</div>
										</div>
									</div>

                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <!-- Script -->
<script type='text/javascript'>
	$(document).ready(function () {

		// Initialize
		$('.ratingbar').rating({
			showCaption: false,
			showClear: false,
			size: 'sm'
		});

		// Rating change
		$('.ratingbar').on('rating:change', function (event, value, caption) {
			var id = this.id;
			var splitid = id.split('_');
			var izvajalecid = splitid[1];

			$.ajax({
				url: '<?= base_url() ?>index.php/ocena/updateRating',
				type: 'post',
				data: {
					izvajalecid: izvajalecid,
					rating: value,
				},
				success: function (response) {
					$('#averagerating_' + izvajalecid).text(response);
				}
			});
		});
	});
</script>