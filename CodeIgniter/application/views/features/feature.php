   <!--================Hero Banner Area Start =================-->
   <section class="hero-banner d-flex align-items-center">
        <div class="container text-center">
            <h2>Posts</h2>
        </div>
    </section>
    <?php if($checkrole == "instruktor") {
            echo '<div class="container text-center alert alert-primary">';
            echo '<a href="' . site_url('pages/view/post') . '">Create New Post</a>';
            echo '</div>';
        }
        ?>
    <!--================Hero Banner Area End =================-->

    <div class="row justify-content-center">
	<div class="col-12 col-md-10 col-lg-8">
		<div class="card card-sm">
			<div class="card-body row no-gutters align-items-center">
				<div class="col-auto">
					<i class="fa fa-search h4 text-body"></i>
				</div>
				<!--end of col-->
				<div class="col">
					<input class="form-control form-control-lg form-control-borderless" id="textarea" type="search"
						   placeholder="Search posts or courses">
				</div>
				<!--end of col-->
				<div class="col-auto">
					<button class="btn btn-lg btn-success" id="searchbutton" type="submit">Search</button>
				</div>
				<!--end of col-->
			</div>
		</div>
	</div>
	<!--end of col-->
</div>

        <!--================Service  Area =================-->
    <section class="service-area" style="padding-top: 2%">
        <div class="container">
            <div class="row">
            <?php if (count($posts)): ?>
                <?php foreach ($posts as $post): ?>
                        <!-- Single service -->
                    <div class="col-md-6 col-lg-4" style="padding-top: 2px; padding-bottom: 2px;">
                        <div class="single-service">
                            <div class="service-icon">
                                <i class="ti-pencil-alt"></i>
                            </div>
                            <div class="service-content">
                                <h5><?php echo $post->ime?></h5>
                                <p><?php echo $post->objava?></p>
                                <a href="<?php echo site_url('features/view/'.$post->id);?>">Read More</a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="alert alert-primary align-items-center" role="alert">
                    No posts in the Database!
                </div>
            <?php endif; ?> 
            </div>
        </div>
    </section>



<!-- Script -->
<script type='text/javascript'>
    $(document).ready(function () {
		$('#searchbutton').click(function (event, value, caption) {
			var text = $("#textarea").val();
			if (text == '') {
				alert("Please review your search parameters");
			} else {
				$.ajax({
					url: '<?= base_url() ?>index.php/features/searchfor',
					type: 'post',
					dataType: "html",
					data: {
						text: text,
					},
					success: function (response) {
						$('body').html(response);
					},
					error: function (result) {
						$('body').html("err");
					},
					beforeSend: function (d) {
						$('body').html("Searching...");
					}
				});
			}
		})
	});
</script>
