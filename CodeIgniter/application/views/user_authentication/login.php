<section style="background: #214a80;">
    <div class="login-dark">
		<?php

			echo form_open('user_authentication/signin');

			echo '<div class="form-group"><label for="email">email</label>';
			echo "</br>";
			$data1 = array(
					'type' => 'email',
					'name' => 'email',
					'class' => 'form-control item'
			);
			echo form_input($data1);
			echo "<div class='error_msg'>";

			echo "</div>";
			echo "</br>";

			echo '<div class="form-group"><label for="geslo">Geslo</label>';
			echo "<br/>";
			$data2 = array(
					'type' => 'password',
					'name' => 'geslo',
					'class' => 'form-control item'
			);
			echo form_password($data2);
			
			echo "</br>";
			$data3 = array(
					'type' => 'submit',
					'name' => 'submit',
					'class' => 'btn btn-primary btn-block',
					'value' => 'Sign In',
			);
			echo form_submit($data3);
			echo form_close();
			?>
        
		</div>

		<a href="<?php echo site_url('user_authentication/show'); ?>">Don't have an account? Sign Up!</a>
</section>