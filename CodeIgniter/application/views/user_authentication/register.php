<section style="background: #214a80;">
    <div class="login-dark">
		<?php

			echo form_open('user_authentication/signup');

			echo '<div class="form-group"><label for="name">Ime</label>';
			echo "</br>";
			$data1 = array(
					'type' => 'text',
					'name' => 'ime',
					'class' => 'form-control item'
			);
			echo form_input($data1);
			echo "<div class='error_msg'>";

			echo "</div>";
			echo "</br>";

			echo '<div class="form-group"><label for="priimek">Priimek</label>';
			echo "</br>";
			$data2 = array(
					'type' => 'text',
					'name' => 'priimek',
					'class' => 'form-control item'
			);
			echo form_input($data2);
			echo "<div class='error_msg'>";

			echo "</div>";
			echo "<br/>";

			echo '<div class="form-group"><label for="telefon">Telefon</label>';
			echo "</br>";
			$data3 = array(
					'type' => 'text',
					'name' => 'telefon',
					'class' => 'form-control item'
			);
			echo form_input($data3);
			echo "<div class='error_msg'>";

			echo "</div>";
			echo "</br>";

			echo '<div class="form-group"><label for="email">Email</label>';
			echo "<br/>";
			$data4 = array(
					'type' => 'email',
					'name' => 'email',
					'class' => 'form-control item'
			);
			echo form_input($data4);
			echo "<br/>";
			echo '<div class="form-group"><label for="geslo">Geslo</label>';
			echo "<br/>";
			$data5 = array(
					'type' => 'password',
					'name' => 'geslo',
					'class' => 'form-control item'
			);
			echo form_password($data5);
			echo "<br/>";

			echo '<div class="form-select"><label for="role">Role</label>';
			echo "</br>";
		
		echo '<select class = form-select aria-label="Default select example" name="role">
			<option selected>Izberi Role</option>
			<option value="instruktor">Instruktor</option>
			<option value="student">Student</option>
		</select>';
			echo "</br>";
			$data7 = array(
					'type' => 'submit',
					'name' => 'submit',
					'class' => 'btn btn-primary btn-block',
					'value' => 'Sign Up',
			);
			echo form_submit($data7);
			echo form_close();
			?>
        
		</div>
		</br></br></br></br></br>
	

		<a href="<?php echo site_url('user_authentication/index'); ?>">Already have an account? Sign In!</a>
</section>