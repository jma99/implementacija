<?php
if (!isset($this->session->userdata['logged_in'])) {
	$data['message_display'] = 'Signin to view this page!';
	$this->load->view('user_authentication/login', $data);
	return;
} ?>
<section style="background: #214a80; padding-top: 10%;">
<div class="col" style="width: 60%; margin: auto">
	<div class="col">
		<div class="block-heading" align="center">
			<h2 class="text-info">Objava</h2>
			<p><?php echo "<div class='error_msg'>";
				echo validation_errors();
				echo "</div>";
				if (isset($error_message)) {
					echo $error_message;
				}; ?></p>
		</div>
	</div>

	<?php echo form_open('features/post') ?>

	<div class="form-group" id="form"><label for="objava">Title</label>
		<br/>
		<?php
		$data2 = array(
				'type' => 'text',
				'name' => 'objava',
				'class' => 'form-control item'
		);
		echo form_input($data2); ?>
		<br/>
	</div>
	<select class="form-select" aria-label="Default select example" name="predmet">
        <option selected>Select course</option>   
        <?php foreach ($courses as $course): ?>
            <option><?php echo $course->ime ?></option>
        <?php endforeach; ?>
	</select><br><br>
	<div class="upload_btn">
	<?php
	$data5 = array(
		'type' => 'submit',
		'name' => 'submit',
		'class' => 'btn btn-primary btn-block',
		'value' => 'Objavi',
	);
	echo form_submit($data5);
	echo form_close();
	?>
</div><br>
</section>