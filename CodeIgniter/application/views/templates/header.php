<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="<?php echo base_url()?>assets/img/favicon.png" type="image/png">
    <title><?php if (isset($title)) echo $title; ?></title>
    

    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/vendors/animate-css/animate.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- main css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href='<?= base_url() ?>assets/bootstrap-star-rating/css/star-rating.min.css' type='text/css' rel='stylesheet'>
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/rating_style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/searchbar_style.css">
    <script src='<?php echo base_url()?>assets/js/jquery-3.5.1.js' type='text/javascript'></script>

	<script src='<?= base_url() ?>assets/bootstrap-star-rating/js/star-rating.min.js' type='text/javascript'></script>




</head>
<body>

    <!--================Header Menu Area =================-->
    <header class="header_area">	
        <div class="main_menu">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <a class="navbar-brand logo_h" href="<?php echo site_url('pages/view/index'); ?>">Nauči.si</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                        <ul class="nav navbar-nav menu_nav ml-auto">
                            <?php if ($logiran == false) {
                               
                               echo '<li class="nav-item"><a class="nav-link" href="'; echo site_url('pages/view/index') . '"' . '>Home</a></li>';
                               echo '<li class="nav-item"><a class="nav-link" href="'; echo site_url('pages/view/aboutus') . '"' . '>About Us</a></li>';
                               echo '<li class="nav-item"><a class="nav-link" href="'; echo site_url('pages/view/contact') . '"' . '>Contact</a></li>';
                            
                            } else {
                            
                               echo '<li class="nav-item"><a class="nav-link" href="'; echo site_url('pages/view/index') . '"' . '>Home</a></li>';
                               echo '<li class="nav-item"><a class="nav-link" href="'; echo site_url('pages/view/aboutus') . '"' . '>About Us</a></li>';
                               echo '<li class="nav-item"><a class="nav-link" href="'; echo site_url('pages/view/contact') . '"' . '>Contact</a></li>';
                               echo '<li class="nav-item"><a class="nav-link" href="'; echo site_url('features/index') . '"' . '>Posts</a></li>';

                               if ($checkrole == 'student') {
                                echo '<li class="nav-item"><a class="nav-link" href="'; echo site_url('ocena/index') . '"' . '>Grade</a></li>';
                               }

                            }
                            ?>

                        <?php if ($logiran == false) {
                               
                               echo '<li class="nav-item"><a class="nav-link" href="'; echo site_url("user_authentication/index") . '"' . '>Login</a></li>';
                               echo '<li class="nav-item"><a class="nav-link" href="'; echo site_url("user_authentication/show") . '"' . '>Register</a></li>';

                            
                            } else {
                            
                               echo '<li class="nav-item"><a class="nav-link" href="'; echo site_url("user_authentication/logout") . '"' . '>Logout</a></li>';

                            }
                            ?>
                        </ul>
                    </div> 
                </div>
            </nav>
        </div>
    </header>